<?php  

<head>
      // Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta property="og:title" content="Do Zen Ideas!"/>
        <meta property="og:title" content="Klare Kommunikation, nachhaltig gestaltet."/>
      	<meta property="og:image" content="http://dozenideas.de/img/og-logo.jpg"/>


      <title>Do Zen Ideas!</title>
      // Stylesheets
       
        <link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">


        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/dozen.css" media="screen">
        <link rel="stylesheet" type="text/css" href="css/kontakt.css" media="screen">

      
</head>
    
    
?>